<?php

class ApiTest extends \PHPUnit_Framework_TestCase
{

    private $_client;

    protected function setUp()
    {
        $conf = parse_ini_file(__DIR__ . '/../config/app.ini', true);
        $this->_client = new GuzzleHttp\Client([
            'base_url' => $conf['dev']['base_url'],
            'defaults' => [
                'exceptions' => false
            ]
        ]);
    }

    public function testError()
    {
        $res = $this->_client->get('/');
        $this->assertEquals(404, $res->getStatusCode());
        $this->assertEquals('{"msg":"the requested page could not be found"}', $res->getBody(true));
        
        $res = $this->_client->get('/test/-1');
        $this->assertEquals(404, $res->getStatusCode());
        $this->assertEquals('{"msg":"the requested page could not be found"}', $res->getBody(true));
    }

    public function testDeleteGame()
    {
        // delete not-existing
        $res = $this->_client->delete('/games/-1');
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(404, $res->getStatusCode());
        $this->assertEquals(- 1, $res_decoded['id']);
        $this->assertEquals('game was not found', $res_decoded['msg']);
        
        // create
        $res = $this->_client->post('/games');
        $res_decoded = json_decode($res->getBody(true), true);
        $id = $res_decoded['id'];
        
        // delete
        $res = $this->_client->delete('/games/' . $id);
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertEquals($id, $res_decoded['id']);
        $this->assertEquals('game was deleted', $res_decoded['msg']);
        
        // check getting what was deleted
        $res = $this->_client->get('/games/' . $id);
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(404, $res->getStatusCode());
        $this->assertEquals($id, $res_decoded['id']);
        $this->assertEquals('game was not found', $res_decoded['msg']);
    }

    public function testGetGames()
    {
        // create
        $res = $this->_client->post('/games');
        $res_decoded = json_decode($res->getBody(true), true);
        $id = $res_decoded['id'];
        
        // get
        $res = $this->_client->get('/games');
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertNotEmpty($res_decoded[$id]);
        
        // delete
        $res = $this->_client->delete('/games/' . $id);
    }

    public function testGetGameId()
    {
        // not-existing
        $res = $this->_client->get('/games/-1');
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(404, $res->getStatusCode());
        $this->assertEquals(- 1, $res_decoded['id']);
        $this->assertEquals('game was not found', $res_decoded['msg']);
        
        // create
        $res = $this->_client->post('/games');
        $res_decoded = json_decode($res->getBody(true), true);
        $id = $res_decoded['id'];
        
        // get
        $res = $this->_client->get('/games/' . $id);
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertNotEmpty($res_decoded['id']);
        $this->assertNotEmpty($res_decoded['word']);
        $this->assertNotEmpty($res_decoded['tries']);
        $this->assertNotEmpty($res_decoded['status']);
        
        // delete
        $res = $this->_client->delete('/games/' . $id);
    }

    public function testPostGames()
    {
        // create
        $res = $this->_client->post('/games');
        $this->assertEquals(200, $res->getStatusCode());
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertGreaterThan(0, $res_decoded['id']);
        $this->assertEquals('new game was started', $res_decoded['msg']);
        
        // delete
        $res = $this->_client->delete('/games/' . $res_decoded['id']);
    }

    public function testPostGameId()
    {
        // not-existing
        $options = [
            'body' => [
                'char' => 'a'
            ]
        ];
        $res = $this->_client->post('/games/-1', $options);
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(404, $res->getStatusCode());
        $this->assertEquals(- 1, $res_decoded['id']);
        $this->assertEquals('game was not found', $res_decoded['msg']);
        
        // create
        $res = $this->_client->post('/games');
        $res_decoded = json_decode($res->getBody(true), true);
        $id = $res_decoded['id'];
        
        // not-letter
        $options = [
            'body' => [
                'char' => 1
            ]
        ];
        $res = $this->_client->post('/games/' . $id, $options);
        $res_decoded = json_decode($res->getBody(true), true);
        $this->assertEquals(200, $res->getStatusCode());
        $this->assertEquals($id, $res_decoded['id']);
        $this->assertEquals('not valid character', $res_decoded['msg']);
        
        // delete
        $res = $this->_client->delete('/games/' . $id);
    }
}