<?php

class WordTest extends \PHPUnit_Framework_TestCase
{

    private $_word;

    protected function setUp()
    {
        $this->_word = new Word();
    }

    public function testGetWord()
    {
        $word = $this->_word->GetWord();
        $this->assertNotEmpty($word);
    }
}