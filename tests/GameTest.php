<?php

class GameTest extends \PHPUnit_Framework_TestCase
{

    private $_game;

    protected function setUp()
    {
        $this->_game = new Game();
    }

    protected function tearDown()
    {
        $this->_game->DeleteGame();
    }

    public function testCreateGame()
    {
        $id = $this->_game->CreateGame();
        $this->assertGreaterThan(0, $id);
        $this->_game = Game::find($id);
        
        $this->assertEquals(Game::TRIES, $this->_game->tries);
        $this->assertNotEmpty($this->_game->word);
    }

    public function testViewGames()
    {
        $id = $this->_game->CreateGame();
        $this->_game = Game::find($id);
        $this->_game->word = 'lazy';
        $this->_game->save();
        
        $games = $this->_game->GetGames();
        $this->assertNotEmpty($games);
        $this->assertNotEmpty($games[$this->_game->id]);
        $this->assertEquals(Game::HIDDEN_LETTER . Game::HIDDEN_LETTER . Game::HIDDEN_LETTER . Game::HIDDEN_LETTER, $games[$this->_game->id]['word']);
        $this->assertEquals(Game::TRIES, $games[$this->_game->id]['tries']);
        $this->assertEquals('busy', $games[$this->_game->id]['status']);
    }

    public function testGuessLetter()
    {
        $id = $this->_game->CreateGame();
        $this->_game = Game::find($id);
        $this->_game->word = 'lazy';
        $this->_game->save();
        
        $this->assertEquals($this->_game->GuessLetter(''), 'not valid character');
        $this->assertEquals($this->_game->GuessLetter(1), 'not valid character');
        $this->assertEquals($this->_game->GuessLetter('aa'), 'not valid character');
        $this->assertTrue($this->_game->GuessLetter('a'));
        $this->_game->reload();
        $this->assertEquals(Game::TRIES, $this->_game->tries);
        $this->assertTrue($this->_game->GuessLetter('b'));
        $this->_game->reload();
        $this->assertEquals(Game::TRIES - 1, $this->_game->tries);
        $this->assertEquals($this->_game->GuessLetter('a'), 'this letter was already tried');
    }

    public function testWinningGame()
    {
        $id = $this->_game->CreateGame();
        $this->_game = Game::find($id);
        $this->_game->word = 'aaz';
        $this->_game->save();
        
        // test status winning
        $this->_game->GuessLetter('a');
        $this->_game->GuessLetter('z');
        $this->_game->reload();
        $this->assertEquals(Game::SUCCESS, $this->_game->status);
        
        // test entering characters after game
        $this->assertEquals($this->_game->GuessLetter('b'), 'game is already finished');
    }

    public function testLosingGame()
    {
        $id = $this->_game->CreateGame();
        $this->_game = Game::find($id);
        $this->_game->word = 'bbc';
        $this->_game->save();
        
        // test status losing
        $this->_game->GuessLetter('a');
        $this->_game->GuessLetter('x');
        $this->_game->GuessLetter('y');
        $this->_game->GuessLetter('z');
        $this->_game->GuessLetter('f');
        $this->_game->GuessLetter('g');
        $this->_game->GuessLetter('h');
        $this->_game->GuessLetter('o');
        $this->_game->GuessLetter('p');
        $this->_game->GuessLetter('r');
        $this->_game->GuessLetter('q');
        $this->_game->reload();
        $this->assertEquals(Game::FAIL, $this->_game->status);
        
        // test entering characters after game
        $this->assertEquals($this->_game->GuessLetter('b'), 'game is already finished');
    }

    public function testGetWinningGame()
    {
        $id = $this->_game->CreateGame();
        $this->_game = Game::find($id);
        $this->_game->word = 'god';
        $this->_game->save();
        
        $game_details = $this->_game->GetGame();
        $this->assertEquals(Game::HIDDEN_LETTER . Game::HIDDEN_LETTER . Game::HIDDEN_LETTER, $game_details['word']);
        $this->assertEquals(Game::TRIES, $game_details['tries']);
        $this->assertEquals('busy', $game_details['status']);
        
        $this->_game->GuessLetter('a');
        $this->_game->reload();
        $game_details = $this->_game->GetGame();
        $this->assertEquals(Game::HIDDEN_LETTER . Game::HIDDEN_LETTER . Game::HIDDEN_LETTER, $game_details['word']);
        $this->assertEquals(Game::TRIES - 1, $game_details['tries']);
        $this->assertEquals('busy', $game_details['status']);
        
        $this->_game->GuessLetter('o');
        $this->_game->reload();
        $game_details = $this->_game->GetGame();
        $this->assertEquals(Game::HIDDEN_LETTER . 'o' . Game::HIDDEN_LETTER, $game_details['word']);
        $this->assertEquals(Game::TRIES - 1, $game_details['tries']);
        $this->assertEquals('busy', $game_details['status']);
        
        $this->_game->GuessLetter('g');
        $this->_game->GuessLetter('d');
        $this->_game->reload();
        $game_details = $this->_game->GetGame();
        $this->assertEquals('god', $game_details['word']);
        $this->assertEquals(Game::TRIES - 1, $game_details['tries']);
        $this->assertEquals('success', $game_details['status']);
    }

    public function testGetLosingGame()
    {
        $id = $this->_game->CreateGame();
        $this->_game = Game::find($id);
        $this->_game->word = 'god';
        $this->_game->save();
        
        $this->_game->GuessLetter('a');
        $this->_game->GuessLetter('x');
        $this->_game->GuessLetter('y');
        $this->_game->GuessLetter('z');
        $this->_game->GuessLetter('f');
        $this->_game->GuessLetter('g');
        $this->_game->GuessLetter('h');
        $this->_game->GuessLetter('o');
        $this->_game->GuessLetter('p');
        $this->_game->GuessLetter('r');
        $this->_game->GuessLetter('q');
        $this->_game->GuessLetter('s');
        $this->_game->GuessLetter('t');
        $this->_game->reload();
        $game_details = $this->_game->GetGame();
        $this->assertEquals('go.', $game_details['word']);
        $this->assertEquals(Game::TRIES - 11, $game_details['tries']);
        $this->assertEquals('fail', $game_details['status']);
    }
}