<?php

class Game extends ActiveRecord\Model
{

    static $table_name = 'games';

    private $_game;

    const TRIES = 11;

    const BUSY = 0;

    const FAIL = 1;

    const SUCCESS = 2;

    const HIDDEN_LETTER = '.';

    private function _findGame()
    {
        try {
            $this->_game = Game::find($this->id);
            return true;
        } catch (\ActiveRecord\RecordNotFound $e) {
            return false;
        }
    }

    /**
     *
     * @return integer
     */
    public function CreateGame()
    {
        $word = new Word();
        $this->_game = Game::create(array(
            'word' => $word->GetWord(),
            'tries' => self::TRIES
        ));
        return $this->_game->id;
    }

    /**
     *
     * @return boolean
     */
    public function DeleteGame()
    {
        if (empty($this->_findGame()))
            return false;
        $this->_game->delete();
        return true;
    }

    /**
     *
     * @return array
     */
    public function GetGames()
    {
        $games = Game::find('all');
        foreach ($games as $game) {
            $this->id = $game->id;
            $this->_findGame();
            $data[$game->id] = $this->GetGame();
        }
        return $data;
    }

    /**
     *
     * @return array
     */
    public function GetGame()
    {
        if (empty($this->_findGame()))
            return false;
        $word = $this->_game->word;
        $simplified_word_array = array_unique(str_split($word));
        $unguessed = array_diff($simplified_word_array, str_split($this->_game->letters));
        $result['id'] = $this->id;
        $result['word'] = str_replace($unguessed, self::HIDDEN_LETTER, $word);
        $result['tries'] = $this->_game->tries;
        if ($this->_game->status == self::BUSY)
            $result['status'] = 'busy';
        if ($this->_game->status == self::FAIL)
            $result['status'] = 'fail';
        if ($this->_game->status == self::SUCCESS)
            $result['status'] = 'success';
        return $result;
    }

    public function GuessLetter($letter)
    {
        if (empty($this->_findGame()))
            return false;
        if ($this->_game->status != 0)
            return 'game is already finished';
        if (! preg_match('/^[a-z]$/', $letter))
            return 'not valid character';
        if (array_intersect(str_split($this->_game->letters), str_split($letter)))
            return 'this letter was already tried';
        
        $this->_game->letters .= $letter;
        
        // check guess wrong
        if (empty(array_intersect(str_split($this->_game->word), str_split($letter))))
            if (-- $this->_game->tries == 0)
                $this->_game->status = self::FAIL;
            
            // check guess right
        $simplified_word_array = array_unique(str_split($this->_game->word));
        if (array_intersect($simplified_word_array, str_split($this->_game->letters)) == $simplified_word_array)
            $this->_game->status = self::SUCCESS;
        
        $this->_game->save();
        return true;
    }
}