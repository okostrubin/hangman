<?php

class Word extends ActiveRecord\Model

{

    static $table_name = 'words_english';

    public function GetWord()
    {
        $max_id = Word::find('first', array(
            'order' => 'id desc'
        ));
        $id = rand(1, $max_id->id);
        $word = Word::find($id);
        return $word->word;
    }
}