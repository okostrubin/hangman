CREATE DATABASE IF NOT EXISTS `hangman`;
USE `hangman`;

CREATE TABLE IF NOT EXISTS `games` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(30) NOT NULL DEFAULT '',
  `letters` varchar(26) NOT NULL DEFAULT '',
  `tries` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - busy, 1 - fail, 2 - success',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

INSERT INTO `games` (`id`, `word`, `letters`, `tries`, `status`) VALUES
(1, 'fetch', 'fetch', 11, 2),
(2, 'underclothe', 'abxyzdefghuqrswv', 0, 1),
(3, 'sexually', 'yabcselwxu', 8, 2),
(4, 'ultrasharp', 'uaxyz', 8, 0),
(5, 'colocynth', 'ao', 10, 0),
(6, 'escheating', 'e', 11, 0),
(7, 'goliathized', '', 11, 0),
(8, 'structuralises', '', 11, 0),
(9, 'mesosome', '', 11, 0),
(10, 'affluencies', '', 11, 0);