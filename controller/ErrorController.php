<?php
use Symfony\Component\HttpFoundation\JsonResponse;

$app->error(function (\Exception $e, $code)
{
    switch ($code) {
        case 404:
            $message = 'the requested page could not be found';
            break;
        default:
            $message = 'Error: ' . $e;
    }
    $arr = array(
        'msg' => $message
    );
    return new JsonResponse($arr, $code, array(
        'Content-Type' => 'application/json'
    ));
});