<?php
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

$app->get('/games', function () use($app)
{
    $game = new Game();
    return new JsonResponse($game->GetGames(), 200, array(
        'Content-Type' => 'application/json'
    ));
});

$app->get('/games/{id}', function ($id) use($app)
{
    $game = new Game();
    $game->id = $id;
    $response = $game->GetGame();
    
    if ($response)
        $status = 200;
    else {
        $response = array(
            'id' => $id,
            'msg' => 'game was not found'
        );
        $status = 404;
    }
    return new JsonResponse($response, $status, array(
        'Content-Type' => 'application/json'
    ));
});

$app->post('/games', function () use($app)
{
    $game = new Game();
    $response = array(
        'id' => $game->CreateGame(),
        'msg' => 'new game was started'
    );
    return new JsonResponse($response, 200, array(
        'Content-Type' => 'application/json'
    ));
});

$app->post('/games/{id}', function ($id, Request $request) use($app)
{
    $game = new Game();
    $game->id = $id;
    $char = $request->get('char');
    $result = $game->GuessLetter($char);
    
    if ($result === true) {
        $response = array(
            'id' => $id,
            'msg' => 'guess attempt was accepted'
        );
        $status = 200;
    } elseif ($result === false) {
        $response = array(
            'id' => $id,
            'msg' => 'game was not found'
        );
        $status = 404;
    } else {
        $response = array(
            'id' => $id,
            'msg' => $result
        );
        $status = 200;
    }
    return new JsonResponse($response, $status, array(
        'Content-Type' => 'application/json'
    ));
});

$app->delete('/games/{id}', function ($id) use($app)
{
    $game = new Game();
    $game->id = $id;
    
    if ($game->DeleteGame()) {
        $response = array(
            'id' => $id,
            'msg' => 'game was deleted'
        );
        $status = 200;
    } else {
        $response = array(
            'id' => $id,
            'msg' => 'game was not found'
        );
        $status = 404;
    }
    return new JsonResponse($response, $status, array(
        'Content-Type' => 'application/json'
    ));
});