# Requirements
PHP Version: PHP 5.5
MySQL 5

# Installation
Import /information_schema/trycatch.sql
Modify /config/app.ini
 [db][dsn] database connection details
 [dev][base_url] base url for running tests

# Running application
POST      /games       Start a new game
GET       /games       Overview of all games
GET       /games/:id   Game details by id
POST      /games/:id   Guessing a letter, POST body:
 char=a
DELETE    /games/:id   Delete address by id

# Running tests
phpunit -c ./tests/phpunit.xml ./tests

# Used libraries
Silex http://silex.sensiolabs.org/
PHPUnit https://phpunit.de/
Guzzle http://guzzle.readthedocs.org/
