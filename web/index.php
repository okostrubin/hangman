<?php
require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

// Config
$app['debug'] = false;
$app['conf'] = $app->share(function () use($app)
{
    $data = parse_ini_file(__DIR__ . '/../config/app.ini', true);
    return $data;
});

// PHPActiveRecord
ActiveRecord\Config::initialize(function ($cfg) use($app)
{
    $cfg->set_model_directory(__DIR__ . '/../model');
    $cfg->set_connections(array(
        'development' => $app['conf']['db']['dsn']
    ));
    
    $cfg->set_default_connection('development');
});

// Controllers
foreach (glob(__DIR__ . "/../controller/*.php") as $filename) {
    require_once $filename;
}

$app->run();
